package disk

type ErrD struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func (e *ErrD) Error() string {
	return e.Message
}

func ErrorDisk(code int, msg string) *ErrD {
	return &ErrD{code, msg}
}
