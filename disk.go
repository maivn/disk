package disk

import (
	"net/url"
	"net/http"
	"io/ioutil"
	"os"
	"io"
)

type disk interface {
	GetMeta(publicUrl url.URL) (meta Meta, err error)
	GetFile(publicUrl url.URL, dir, file string) (path string, err error)
}
type Meta interface {
	GetFileName() string
}

func NewDisk(publicUrl url.URL) (disk disk, err error) {
	switch host := publicUrl.Host; host {
	case "yadi.sk":
		disk = &YandexDisk{}
		return
	default:
		return nil, ErrorDisk(1, "File shared unsupported!!!")
	}
}

func getRequest(u *url.URL) (data []byte, err error) {
	resp, err := http.Get(u.String())
	if err != nil {
		err = ErrorDisk(2, "Error get request: "+err.Error())
		return
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		err = ErrorDisk(4, "Error ioutil readAll: "+err.Error())
		return
	}
	return
}

func getFileRequest(u *url.URL, path string) (err error) {
	resp, err := http.Get(u.String())
	if err != nil {
		err = ErrorDisk(3, "Error get file request: "+err.Error())
		return
	}
	defer resp.Body.Close()

	out, err := os.Create(path)
	if err != nil {
		err = ErrorDisk(5, "Error create file by path: "+path+": "+err.Error())
		return
	}
	defer out.Close()

	_, err = io.Copy(out, resp.Body)
	if err != nil {
		err = ErrorDisk(6, "Error copy data to file by path: "+path+": "+err.Error())
		return
	}
	return
}
