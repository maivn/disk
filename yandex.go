package disk

import (
	"net/url"
	"encoding/json"
	"path/filepath"
	"os"
)

type YandexDisk struct {
}

type YandexMeta struct {
	Name string
}

type respDownloadYandex struct {
	Href      string `json:"href"`
	Method    string `json:"method"`
	Templated bool   `json:"templated"`
}

func (y *YandexMeta) GetFileName() string {
	return y.Name
}

func (y *YandexDisk) GetMeta(publicUrl url.URL) (meta Meta, err error) {
	apiUrl := url.URL{
		Scheme:   "https",
		Host:     "cloud-api.yandex.net",
		Path:     "/v1/disk/public/resources",
		RawQuery: "public_key=" + publicUrl.String(),
	}
	data, err := getRequest(&apiUrl)
	if err != nil {
		return
	}
	meta = &YandexMeta{}
	err = json.Unmarshal(data, meta)
	if err != nil {
		err = ErrorDisk(7, "Error (GetMeta) json unmarshal: "+err.Error())
	}
	return
}

func (y *YandexDisk) GetFile(publicUrl url.URL, dir, file string) (path string, err error) {
	apiUrl := url.URL{
		Scheme:   "https",
		Host:     "cloud-api.yandex.net",
		Path:     "/v1/disk/public/resources/download",
		RawQuery: "public_key=" + publicUrl.String(),
	}
	data, err := getRequest(&apiUrl)
	if err != nil {
		return
	}

	var respYa respDownloadYandex
	err = json.Unmarshal(data, &respYa)
	if err != nil {
		err = ErrorDisk(8, "Error (GetFile) json unmarshal: "+err.Error())
		return
	}

	href, err := url.Parse(respYa.Href)
	if err != nil {
		err = ErrorDisk(9, "Error (GetFile) url parse: "+err.Error())
		return
	}

	path = filepath.Join(dir, file)

	_, err = os.Stat(dir)
	if os.IsNotExist(err) {
		os.MkdirAll(dir, os.ModePerm)
	} else if err != nil {
		err = ErrorDisk(11, "Error (GetFile) os stat: "+err.Error())
		return
	}

	err = getFileRequest(href, path)
	return
}
